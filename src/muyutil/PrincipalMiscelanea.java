/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package muyutil;

import java.io.IOException;

/**
 *
 * @author Pablo
 */
public class PrincipalMiscelanea {
    public static void main(String[] args) throws IOException {
        double num;
        do {
            num = Miscelanea.leeNumero("entra un valor numérico");
            System.out.println("hay introducido " + Miscelanea.euro(num)
            + " " + Miscelanea.posa2d(num));
        } while (num > 0);
    }    
}
