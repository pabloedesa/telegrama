/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package usaTelegrama;

import java.io.IOException;
import muyutil.Miscelanea;

/**
 *
 * @author Pablo
 */
public class PrincipalTelegrama {
    public static void main(String[] args) throws IOException {
        Telegrama telegrama = new Telegrama();
        telegrama.setTexto(Miscelanea.leeCadena("Entra el texto:"));
        telegrama.setTipo(Miscelanea.leeCadena("Entra el tipo Ordinario / Urgente:"));
        System.out.println("el telegrama vale " + telegrama.getCoste());
        System.out.println("El telegrama es\n" + telegrama);
    }    
}
